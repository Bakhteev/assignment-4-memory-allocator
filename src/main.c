#include "./mem_internals.h"
#include "./mem.h"
#include <stdio.h>

#define HEAP_SIZE 6000
#define SIZE_123 123
#define SIZE_456 456
#define SIZE_768 768
#define SIZE_3697 3697

static struct block_header *get_header(void *block)
{
  return (struct block_header *)(block - offsetof(struct block_header, contents));
}

static void clear_heap(void *heap)
{
  munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}

bool test_1(void)
{

  printf("test 1: Init simple heap\n");

  void *heap = heap_init(HEAP_SIZE);
  if (heap == NULL)
  {
    printf("test 1: failed\n");
    return false;
  }
  debug_heap(stdout, heap);
  clear_heap(heap);
  printf("test 1: passed\n");
  return true;
}

bool test_2(void)
{
  printf("test 2: Free one heap block\n");
  void *heap = heap_init(HEAP_SIZE);
  void *block1 = _malloc(SIZE_123);
  void *block2 = _malloc(SIZE_456);
  debug_heap(stdout, heap);

  struct block_header *header1 = get_header(block1);
  _free(block1);
  if (header1->is_free == true)
  {
    debug_heap(stdout, heap);
    printf("test 2: passed\n");
    _free(block2);
    clear_heap(heap);
    return true;
  }
  else
  {
    debug_heap(stdout, heap);
    printf("test 2: failed\n");
    clear_heap(heap);
    clear_heap(heap);
    return false;
  }
}

bool test_3(void)
{
  printf("test 3:Free some heap blocks \n");
  void *heap = heap_init(HEAP_SIZE);
  void *block1 = _malloc(SIZE_123);
  void *block2 = _malloc(SIZE_456);
  void *block3 = _malloc(SIZE_768);
  debug_heap(stdout, heap);

  struct block_header *header1 = get_header(block1);
  struct block_header *header2 = get_header(block2);
  _free(block1);
  _free(block2);
  if (header1->is_free == true && header2->is_free == true)
  {
    debug_heap(stdout, heap);
    printf("test 3: passed\n");
    _free(block3);
    clear_heap(heap);
    return true;
  }
  else
  {
    debug_heap(stdout, heap);
    printf("test 3: failed\n");
    _free(block3);
    clear_heap(heap);
    return false;
  }
}

bool test_4(void)
{
  printf("test 4:Expand region \n");
  void *heap = heap_init(HEAP_SIZE);
  void *block1 = _malloc(HEAP_SIZE + 1);
  void *block2 = _malloc(SIZE_3697);

  struct block_header *header1 = get_header(block1);
  struct block_header *header2 = get_header(block2);
  bool condition1 = block1 == NULL || header1->capacity.bytes != HEAP_SIZE + 1 || header1->next != header2;
  bool condition2 = block2 == NULL || header2->capacity.bytes != SIZE_3697;
  if (!condition1 && !condition2)
  {
    debug_heap(stdout, heap);
    printf("test 4: passed\n");
    _free(block1);
    _free(block2);
    clear_heap(heap);
    return true;
  }
  else
  {
    debug_heap(stdout, heap);
    printf("test 4: failed\n");
    _free(block1);
    _free(block2);
    clear_heap(heap);
    return false;
  }
}

bool test_5(void)
{
  printf("test 5:Expand region later \n");
  void *heap = heap_init(HEAP_SIZE);
  (void)mmap(HEAP_START + REGION_MIN_SIZE,
             REGION_MIN_SIZE,
             PROT_READ | PROT_WRITE,
             MAP_PRIVATE | MAP_FIXED,
             -1,
             0);
  void *block = _malloc(HEAP_SIZE + 1);
  if (!block)
  {
    debug_heap(stdout, heap);
    printf("test 5: failed\n");
    _free(block);
    clear_heap(heap);
    return false;
  }
  else
  {
    debug_heap(stdout, heap);
    printf("test 5: passed\n");
    _free(block);
    clear_heap(heap);
    return true;
  }
}

int main(void)
{
  printf("tests started");
  int counter = 0;
  test_1() ? counter++ : counter;
  test_2() ? counter++ : counter;
  test_3() ? counter++ : counter;
  test_4() ? counter++ : counter;
  test_5() ? counter++ : counter;
  printf("test %d/5\n", counter);
  return 1;
}